from django.shortcuts import render, get_object_or_404, redirect
import stripe
from django.conf import settings
from payments.models import Item

stripe.api_key = settings.STRIPE_SECRET_KEY


def item_detail(request, item_id):
    item = get_object_or_404(Item, id=item_id)
    return render(request, "main.html", {"item": item})


def buy(request, item_id):
    if request.method == 'POST':
        item = get_object_or_404(Item, id=item_id)
        session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            success_url=settings.DOMAIN_URL + "success/",
            cancel_url=settings.DOMAIN_URL + "cancel/",
            line_items=[
                {
                    "price_data": {
                        'currency': 'usd',
                        'unit_amount': item.price,
                        'product_data': {
                            'name': item.name,
                            'description': item.description
                        }
                    },
                    "quantity": 1
                }
            ],
            mode="payment"
        )
        return redirect(session.url)
