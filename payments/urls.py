from django.urls import path
from .views import buy, item_detail

urlpatterns = [
    path("buy/<int:item_id>/", buy),
    path("item/<int:item_id>/", item_detail),
]