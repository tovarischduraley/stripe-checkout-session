## Stripe checkout session test task

Реализована модель Item, детализация которой доступна по url `/item/{id}/`.

### Запуск приложения

```bash
- git clone git@gitlab.com:tovarischduraley/stripe-checkout-session.git
- cd stripe-checkout-session
- python -m venv venv
- venv\Scripts\activate
- pip install -r requirements.txt
- python manage.py migrate
- python manage.py loaddata dbdata.json
- python manage.py runserver
```

### Запуск приложения с Docker

```bash
- git clone git@gitlab.com:tovarischduraley/stripe-checkout-session.git
- docker-compose up
```

Приложение доступно на Heroku по [ссылке](https://stripe-checkout-session.herokuapp.com/)

В базе создан один Item, детализация которого доступна по url `/item/1/`
